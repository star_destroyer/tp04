using System;
using Xunit;

using tp04;

namespace TP04Test
{
    public class UnitTest1
    {
        [Fact]
        public void AireCarreTest()
        {
            Assert.Equal(4, Program.AireCarre(2));
            Assert.Equal(0.25, Program.AireCarre(0.5));
            Assert.Equal(0, Program.AireCarre(0));
        }

        public void AireRectangleTest()
        {
            Assert.Equal(2, Program.AireRectangle(1,2));
            Assert.Equal(2, Program.AireRectangle(4,0.5));
            Assert.Equal(0.75, Program.AireRectangle(0.5,1.5));
        }
    }
}


